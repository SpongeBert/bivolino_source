﻿using System.Collections.Generic;
using System.Web;

namespace Nop.Plugin.Payments.Ogone.Components
{
    public class PostData
    {
        private readonly SortedDictionary<string, string> _inputs = new SortedDictionary<string, string>();

        public string Url { get; set; }

        public string PostMethod { get; set; }

        public string FName { get; set; }

        public SortedDictionary<string, string> SortedPostFields
        {
            get
            {
                return this._inputs;
            }
        }

        public PostData()
        {
            this.PostMethod = "Post";
            this.FName = "Form1";
        }

        public PostData(string FName)
            : this()
        {
            this.FName = FName;
        }

        public PostData(string FName, string url)
            : this()
        {
            this.FName = FName;
            this.Url = url;
        }

        public void Add(string name, string value)
        {
            this._inputs.Add(name, value);
        }

        public void Post()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write("<html><head>");
            HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", this.FName));
            HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", this.FName, this.PostMethod, this.Url));
            foreach (string key in this._inputs.Keys)
                HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", key, this._inputs[key]));
            HttpContext.Current.Response.Write("</form>");
            HttpContext.Current.Response.Write("</body></html>");
            HttpContext.Current.Response.End();
        }
    }
}
