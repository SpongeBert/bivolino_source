﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Infrastructure;
using Nop.Plugin.Payments.Ogone.Models;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;


namespace Nop.Plugin.Payments.Ogone.Controllers
{
    public class PaymentOgoneController : BasePaymentController
    {
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly OgonePaymentSettings _ogonePaymentSettings;
        private readonly IWorkContext _workContext;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;

        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;

        /******************************************************/
        private readonly IOrderService _orderService;
        private readonly IEncryptionService _encryptionService;
        private readonly IStoreService _storeService;
        private readonly IPaymentService _paymentService;
        private readonly PaymentSettings _paymentSettings;
        private readonly OrderSettings _orderSettings;



        public PaymentOgoneController(ISettingService settingService,
            ILocalizationService localizationService,
            OgonePaymentSettings ogonePaymentSettings,
            IWorkContext workContext,
            IOrderProcessingService orderProcessingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IPriceFormatter priceFormatter,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            ILogger logger,
            IOrderService orderService,
            IEncryptionService encryptionService,
            IStoreService storeService,
            IPaymentService paymentService,
            PaymentSettings paymentSettings,
            OrderSettings orderSettings)
        {
            _settingService = settingService;
            _localizationService = localizationService;
            _ogonePaymentSettings = ogonePaymentSettings;
            _workContext = workContext;
            _orderProcessingService = orderProcessingService;
            _orderTotalCalculationService = orderTotalCalculationService;

            _currencyService = currencyService;
            _currencySettings = currencySettings;
            _priceFormatter = priceFormatter;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _logger = logger;
            _orderService = orderService;
            _encryptionService = encryptionService;
            _storeService = storeService;
            _paymentService = paymentService;
            _paymentSettings = paymentSettings;
            _orderSettings = orderSettings;
        }

        [ChildActionOnly]
        [AdminAuthorize]
        public ActionResult Configure()
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var ogonePaymentSettings = _settingService.LoadSetting<OgonePaymentSettings>(storeScope);

            var model = new ConfigurationModel();
            model.PSPID = _ogonePaymentSettings.PSPID;
            model.AdditionalFee = _ogonePaymentSettings.AdditionalFee;
            model.AcceptUrl = _ogonePaymentSettings.AcceptUrl;
            model.Alias = _ogonePaymentSettings.Alias;
            model.AliasOperation = _ogonePaymentSettings.AliasOperation;
            model.AliasUsage = _ogonePaymentSettings.AliasUsage;
            model.CancelUrl = _ogonePaymentSettings.CancelUrl;
            model.CatalogUrl = _ogonePaymentSettings.CatalogUrl;
            model.CN = _ogonePaymentSettings.CN;
            model.COM = _ogonePaymentSettings.COM;
            model.COMPLUS = _ogonePaymentSettings.COMPLUS;
            model.Currency = _ogonePaymentSettings.Currency;
            model.DeclineUrl = _ogonePaymentSettings.DeclineUrl;
            model.ExceptionUrl = _ogonePaymentSettings.ExceptionUrl;
            model.HomeUrl = _ogonePaymentSettings.HomeUrl;
            model.Language = _ogonePaymentSettings.Language;
            model.OwnerAddress = _ogonePaymentSettings.OwnerAddress;
            model.OwnerZIP = _ogonePaymentSettings.OwnerZIP;
            model.SHASign = _ogonePaymentSettings.SHASign;

            model.ActiveStoreScopeConfiguration = storeScope;
            return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/Configure.cshtml", model);
        }

        [HttpPost]
        [ChildActionOnly]
        [AdminAuthorize]
        public ActionResult Configure(ConfigurationModel model)
        {
            //load settings from store
            if (!((Controller)this).ModelState.IsValid)
                return this.Configure();

            int storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            OgonePaymentSettings ogonePaymentSettings = (OgonePaymentSettings)this._settingService.LoadSetting<OgonePaymentSettings>(storeScope);

            _ogonePaymentSettings.PSPID = model.PSPID;
            _ogonePaymentSettings.AdditionalFee = model.AdditionalFee;
            _ogonePaymentSettings.AcceptUrl = model.AcceptUrl;
            _ogonePaymentSettings.Alias = model.Alias;
            _ogonePaymentSettings.AliasOperation = model.AliasOperation;
            _ogonePaymentSettings.AliasUsage = model.AliasUsage;
            _ogonePaymentSettings.CancelUrl = model.CancelUrl;
            _ogonePaymentSettings.CatalogUrl = model.CatalogUrl;
            _ogonePaymentSettings.CN = model.CN;
            _ogonePaymentSettings.COM = model.COM;
            _ogonePaymentSettings.COMPLUS = model.COMPLUS;
            _ogonePaymentSettings.Currency = model.Currency;
            _ogonePaymentSettings.DeclineUrl = model.DeclineUrl;
            _ogonePaymentSettings.ExceptionUrl = model.ExceptionUrl;
            _ogonePaymentSettings.HomeUrl = model.HomeUrl;
            _ogonePaymentSettings.Language = model.Language;
            _ogonePaymentSettings.OwnerAddress = model.OwnerAddress;
            _ogonePaymentSettings.OwnerZIP = model.OwnerZIP;
            _ogonePaymentSettings.SHASign = model.SHASign;
            _ogonePaymentSettings.OrderIdPrefix = model.OrderIdPrefix;
            _settingService.SaveSetting(_ogonePaymentSettings);

            this._settingService.ClearCache();
            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/Configure.cshtml", model);
        }

       
        [ChildActionOnly]
        public ActionResult PaymentInfo(string Brand)
        {
            var model = new PaymentInfoModel();
            try
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();

                var processPaymentRequest = new ProcessPaymentRequest();
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.SelectedPaymentMethod, _genericAttributeService, _storeContext.CurrentStore.Id);
                processPaymentRequest.CreditCardType = null;
                processPaymentRequest.CreditCardName = null;
                processPaymentRequest.CreditCardNumber = null;
                processPaymentRequest.CreditCardExpireMonth = 0;
                processPaymentRequest.CreditCardExpireYear = 0;
                processPaymentRequest.CreditCardCvv2 = null;

                //Calculate order total            
                var shoppingCartTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart);
                //decimal orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart).Value;
                decimal orderTotal = (shoppingCartTotal != null) ? shoppingCartTotal.Value : 0;

                //string amount = Math.Round((orderTotal * 100), 0).ToString();//22032017 - Old
                string amount = Math.Round(_currencyService.ConvertFromPrimaryStoreCurrency(orderTotal, _workContext.WorkingCurrency) * 100, 0).ToString((IFormatProvider) CultureInfo.InvariantCulture);//22032017 - New
                
                //string currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode; //22032017 - Old
                string currency = _workContext.WorkingCurrency.CurrencyCode;//22032017 - New  //Manoj "EUR";//
                string datumprefix = DateTime.Now.Year.ToString() + "" + DateTime.Now.Month.ToString() + "" + DateTime.Now.Day.ToString();
                string orderID = datumprefix + RandomNumber(1000, 9999).ToString();

                //var model = new PaymentInfoModel();
                model.amount = amount;
                model.curr = currency;

                //model.lang = "nl_be";//24032017 - Old
                model.lang = _workContext.WorkingLanguage.LanguageCulture.ToLower();//24032017 - New

                model.orderid = orderID;
                
                model.pspId = _ogonePaymentSettings.PSPID;
                model.shaSign = _ogonePaymentSettings.SHASign;
                model.urlredirectlayout = "ogonelayout.aspx";
                Brand = "{Brand}";
                model.brand = Brand;
                var storeLocation = EngineContext.Current.Resolve<Nop.Core.IWebHelper>().GetStoreLocation();
               // storeLocation = "http://www.bivoshop.com";
                string data = string.Empty;
                //if (!string.IsNullOrEmpty(Brand))
                //{
                //    if (Brand.ToLower() == "ideal")
                //    {
                //        data = "ACCEPTURL=" + storeLocation + "OgoneResult?ogonereferenceid=" + orderID + "propeerstestsha123!AMOUNT=" + model.amount + "propeerstestsha123!BRAND=" + Brand + "propeerstestsha123!CANCELURL=" + storeLocation + "OgoneCancelpropeerstestsha123!CURRENCY=" + model.curr + "propeerstestsha123!LANGUAGE=" + model.lang + "propeerstestsha123!ORDERID=" + model.orderid + "propeerstestsha123!PM=iDEALpropeerstestsha123!PSPID=" + model.pspId + "propeerstestsha123!";
                //    }
                //    else if (Brand.ToLower() == "ing homepay")
                //    {
                //        data = "ACCEPTURL=" + storeLocation + "OgoneResult?ogonereferenceid=" + orderID + "propeerstestsha123!AMOUNT=" + model.amount + "propeerstestsha123!BRAND=" + Brand + "propeerstestsha123!CANCELURL=" + storeLocation + "OgoneCancelpropeerstestsha123!CURRENCY=" + model.curr + "propeerstestsha123!LANGUAGE=" + model.lang + "propeerstestsha123!ORDERID=" + model.orderid + "propeerstestsha123!PM=ING HomePaypropeerstestsha123!PSPID=" + model.pspId + "propeerstestsha123!";
                //    }
                //    else if (Brand.ToLower() == "kbc online")
                //    {
                //        data = "ACCEPTURL=" + storeLocation + "OgoneResult?ogonereferenceid=" + orderID + "propeerstestsha123!AMOUNT=" + model.amount + "propeerstestsha123!BRAND=" + Brand + "propeerstestsha123!CANCELURL=" + storeLocation + "OgoneCancelpropeerstestsha123!CURRENCY=" + model.curr + "propeerstestsha123!LANGUAGE=" + model.lang + "propeerstestsha123!ORDERID=" + model.orderid +
                //            "propeerstestsha123!PM=KBC Onlinepropeerstestsha123!PSPID=" + model.pspId + "propeerstestsha123!";
                //    }
                //    else
                //    {
                //        data = "ACCEPTURL=" + storeLocation + "OgoneResult?ogonereferenceid=" + orderID + "propeerstestsha123!AMOUNT=" + model.amount + "propeerstestsha123!BRAND=" + Brand + "propeerstestsha123!CANCELURL=" + storeLocation + "OgoneCancelpropeerstestsha123!CURRENCY=" + model.curr + "propeerstestsha123!LANGUAGE=" + model.lang + "propeerstestsha123!ORDERID=" + model.orderid + "propeerstestsha123!PM=creditcardpropeerstestsha123!PSPID=" + model.pspId + "propeerstestsha123!";
                //    }
                //}
                //else
                //{
                data = "ACCEPTURL=" + storeLocation + "OgoneResult?ogonereferenceid=" + orderID + model.shaSign +"AMOUNT=" + model.amount + model.shaSign + "BRAND=" + Brand + model.shaSign + "CANCELURL=" + storeLocation + "OgoneCancel" + model.shaSign + "CURRENCY=" + model.curr + model.shaSign + "LANGUAGE=" + model.lang + model.shaSign + "ORDERID=" + model.orderid +
                           model.shaSign + "PM=creditcard" + model.shaSign + "PSPID=" + model.pspId + model.shaSign;
                //}

                model.HTMLHiddenField = data;
                model.AcceptUrl = _ogonePaymentSettings.AcceptUrl;
                //return View("Nop.Plugin.Payments.Ogone.Views.PaymentOgone.PaymentInfo", model);
                return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/PaymentInfo.cshtml", model);
            }
            catch (Exception ex)
            {
                //_logger.InsertLogEmail(LogLevel.Error, "Ogone PaymentInfo Step", (!string.IsNullOrEmpty(ex.Message) ? ex.Message + " :: " : "") + ex.StackTrace, _workContext.CurrentCustomer);
                return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/PaymentInfo.cshtml", model);
            }
        }


        #region OgonePaymentUrls

        public ActionResult OgoneResult(string OgoneReferenceID)
        {
            var model = new OgoneUrl();
            Session["OgoneReferenceID"] = OgoneReferenceID;
            string storelocation = EngineContext.Current.Resolve<IWebHelper>().GetStoreLocation();
            string url = _settingService.GetSettingByKey("ogonePaymentSettings.AcceptUrl", storelocation);
            model.Url = url;
            model.orderId = OgoneReferenceID;
            model.storeurl = storelocation;
            
            //return RedirectToRoute("CheckoutCompleted",new { } )
            //return (ActionResult)((Controller)this).RedirectToRoute("CheckoutCompleted", new { orderId = orderId });
            return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/OgoneResult.cshtml", model);
        }

        public ActionResult OgoneCancel()
        
        {
            var model = new OgoneUrl();
            string storelocation = EngineContext.Current.Resolve<IWebHelper>().GetStoreLocation();
            string url = _settingService.GetSettingByKey("ogonePaymentSettings.CancelUrl", storelocation);
            model.Url = url;            
            return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/OgoneCancel.cshtml", model);
        }

        //public ActionResult Confirm()
        //{
        //    //validation
        //    var cart = _workContext.CurrentCustomer.ShoppingCartItems
        //         .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
        //         .LimitPerStore(_storeContext.CurrentStore.Id)
        //         .ToList();
        //    if (cart.Count == 0)

        //        return RedirectToRoute("ShoppingCart");

        //    if (_orderSettings.OnePageCheckoutEnabled)
        //        return RedirectToRoute("CheckoutOnePage");

        //    if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
        //        return new HttpUnauthorizedResult();

        //    //model
        //    ViewBag.skipConfirm = true;
            
        //    var model = PrepareConfirmOrderModel(cart);
        //    return View(model);
        //}
        
        #endregion

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            //var validator = new PaymentInfoValidator(_localizationService);
            //var model = new PaymentInfoModel()
            //{
            //    CardholderName = form["CardholderName"],
            //    CardNumber = form["CardNumber"],
            //    CardCode = form["CardCode"],
            //};
            //var validationResult = validator.Validate(model);
            //if (!validationResult.IsValid)
            //    foreach (var error in validationResult.Errors)
            //        warnings.Add(error.ErrorMessage);
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            try
            {
                paymentInfo.CreditCardType = form["CreditCardType"];
                paymentInfo.CreditCardName = form["CardholderName"];
                paymentInfo.CreditCardNumber = form["CardNumber"];
                paymentInfo.CreditCardExpireMonth = int.Parse(form["ExpireMonth"]);
                paymentInfo.CreditCardExpireYear = int.Parse(form["ExpireYear"]);
                paymentInfo.CreditCardCvv2 = form["CardCode"];
            }
            catch { }
            return paymentInfo;
        }

        /******************************************************************************************/
        [NonAction]
        public void ProcessPostBackModel(ReturnModel model)
        {
            if (model == null)
                return;
            int orderId = this.GetOrderId(model.OrderId);
            SortedDictionary<string, string> values = new SortedDictionary<string, string>();
            NameValueCollection nameValueCollection = ((Controller)this).Request.RequestType == "POST" ? ((Controller)this).Request.Form : ((Controller)this).Request.QueryString;
            StringBuilder stringBuilder = new StringBuilder("Ogone Postback Fields:\r\n");
            foreach (string key in (NameObjectCollectionBase)nameValueCollection)
            {
                if (!key.ToUpper().Equals("SHASIGN"))
                    values.Add(key, nameValueCollection[key]);
                stringBuilder.AppendFormat("{0}={1};\r\n", key, nameValueCollection[key]);
            }
            Order orderById = this._orderService.GetOrderById(orderId);

            if (BaseEntity.Equals((BaseEntity)orderById, (BaseEntity)null))
            {
                this._logger.InsertLog((LogLevel)40, "Invalid order id", string.Format("Ogone Postback Error. Order id {0} not found.", orderId), (Customer)null);
            }

            else
            {
                this.AddOrderNote(orderById, stringBuilder.ToString(), false);
                OgonePaymentProcessor paymentProcessor = this._paymentService.LoadPaymentMethodBySystemName("Payments.Ogone") as OgonePaymentProcessor;
                if (paymentProcessor == null || !PaymentExtensions.IsPaymentMethodActive((IPaymentMethod)paymentProcessor, this._paymentSettings)) //|| !paymentProcessor.get_PluginDescriptor().get_Installed())
                    throw new NopException("Ogone Payment module cannot be loaded");
                if (!paymentProcessor.VerifyHashDigest(values, model.ShaSign))
                {
                    this.AddOrderNote(orderById, "Ogone Postback Error. SHA-digest verification failed", false);
                }
                else
                {
                    PaymentStatus paymentStatus = MatridOgoneHelper.GetPaymentStatus(model.Status.ToString(), model.NCError);
                    if (Convert.ToInt32(paymentStatus) <= 20)
                    {
                        if (Convert.ToInt32(paymentStatus) == 10 || Convert.ToInt32(paymentStatus) != 20 || !this._orderProcessingService.CanMarkOrderAsAuthorized(orderById))
                            return;
                        this._orderProcessingService.MarkAsAuthorized(orderById);
                    }
                    else if (Convert.ToInt32(paymentStatus) != 30)
                    {
                        if (Convert.ToInt32(paymentStatus) != 50 || !this._orderProcessingService.CanCancelOrder(orderById))
                            return;
                        this._orderProcessingService.CancelOrder(orderById, true);
                    }
                    else
                    {
                        if (!this._orderProcessingService.CanMarkOrderAsPaid(orderById))
                            return;
                        this._orderProcessingService.MarkOrderAsPaid(orderById);
                    }
                }
            }
        }


        private int GetOrderId(string orderId)
        {
            OgonePaymentSettings ogonePaymentSettings = (OgonePaymentSettings)this._settingService.LoadSetting<OgonePaymentSettings>(((BaseController)this).GetActiveStoreScopeConfiguration(this._storeService, this._workContext));
            return string.IsNullOrEmpty(ogonePaymentSettings.OrderIdPrefix) ? Convert.ToInt32(orderId) : Convert.ToInt32(orderId.Substring(ogonePaymentSettings.OrderIdPrefix.Length));
        }


        [NonAction]
        public void AddOrderNote(Order order, string noteText, bool displayToCustomer)
        {
            ICollection<OrderNote> orderNotes = order.OrderNotes;
            OrderNote orderNote1 = new OrderNote();
            orderNote1.Note = noteText;
            orderNote1.DisplayToCustomer = displayToCustomer;
            orderNote1.CreatedOnUtc = DateTime.UtcNow;
            OrderNote orderNote2 = orderNote1;
            orderNotes.Add(orderNote2);
            this._orderService.UpdateOrder(order);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult AcceptPayment(ReturnModel model)
        {
            if (model == null)
                return (ActionResult)new EmptyResult();
            int orderId = this.GetOrderId(model.OrderId);
            this.ProcessPostBackModel(model);
            return RedirectToRoute("CheckoutCompleted", new { orderId = orderId });
        }

        [ValidateInput(false)]
        public ActionResult CancelPayment(ReturnModel model)
        {
            this.ProcessPostBackModel(model);
            return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/CancelPayment.cshtml",model);
        }

        //[ValidateInput(false)]
        //public ActionResult DeclinePayment(ReturnModel model)
        //{
        //     this.ProcessPostBackModel(model);
        //     return View("~/Plugins/Payments.Ogone/Views/PaymentOgone/DeclinePayment.cshtml");
        // }

       
    }
}
