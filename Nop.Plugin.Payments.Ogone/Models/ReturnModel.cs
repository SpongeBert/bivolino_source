﻿namespace Nop.Plugin.Payments.Ogone.Models
{
    public class ReturnModel
    {
        public string Acceptance { get; set; }
        public string Amount { get; set; }
        public string Brand { get; set; }
        public string CardNo { get; set; }
        public string CN { get; set; }
        public string Currency { get; set; }
        public string NCError { get; set; }
        public string OrderId { get; set; }
        public string PayId { get; set; }
        public string PM { get; set; }
        public int Status { get; set; }
        public string TRXDate { get; set; }
        public string ShaSign { get; set; }       
    }
}
